$(function(){
	$('.btnn').click(function(){
		var input1, input2,errors=[], has_errors=false;
		var error_msg="";
		input1 = $('.email').val();
		input1=input1.replace(/\s/g,"");
		input2 = $('.title').val();
		if( input1.length == 0 || !emailTest(input1)){
		if(input1.length == 0){
			error_msg = 'the email_field is required';
			errors.push(error_msg);
		}
		if(!emailTest(input1)){
			error_msg = 'please enter a valid email address';
			errors.push(error_msg);
		}
		has_errors = true;
		}
		if(input2.length == 0){
			error_msg = 'the password field is required';
			errors.push(error_msg);
			has_errors = true;
			
		}
		if(has_errors){

			alert('some errors occured');
			var display_text = '<ul>';
			for (var i = 0; i < errors.length; i++) {
				display_text += '<li>'+errors[i]+'</li>';
			}
			display_text += '</ul>';
			$('#display-errors').html(display_text);
				return false;

		}else{
		form.submit();
		}
	});
});
	
function emailTest(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}