
$('.btnn').click(function(){
	var input1, input2, input3,errors=[], has_errors=false;
	var error_msg="";
	input1 = $('.email').val();
	input1 =input1.replace(/\s/g,"");
	input2 = $('.title').val();
	input2 =input2.replace(/\s/g,"");
	input3 =$('.address').val();

	if( input1.length == 0 || !emailTest(input1)){

		if(input1.length == 0){
			error_msg = 'the email field is required';
			errors.push(error_msg);
		}
		if(!emailTest(input1)){
			error_msg = 'please enter a valid email address';
			errors.push(error_msg);
		}
		has_errors = true;
	}

	if(input2.length == 0){
		error_msg = 'the title field is required';
		errors.push(error_msg);
		has_errors = true;
		
	}

	if(input3.length == 0){
		error_msg = 'the Home address field is required';
		errors.push(error_msg);
		has_errors = true;
		
	}

	if(has_errors){

		alert('please fill the specified fields with errors');
		var display_text = '<ul>';
		for (var i = 0; i < errors.length; i++) {
			display_text += '<li>'+errors[i]+'</li>';
		}
		display_text += '</ul>'; 
		$('#err-msgs').html(display_text);
		return false;

	}else{
		form.submit();
	}
});

$('#sign-in-submit').click(function(){
	
	var input1, input2, input3,errors=[], has_errors=false;
	var error_msg="";
	input1 = $('#email').val();
	input1 = input1.replace(/\s/g,"");
	input2 = $('#pass').val();
	if( input1.length == 0 || !emailTest(input1)){
		if(input1.length == 0){
			error_msg = 'the email field is required';
			errors.push(error_msg);
		}
		if(!emailTest(input1)){
			error_msg = 'please enter a valid email address';
			errors.push(error_msg);
		}
		has_errors = true;
	}
	if(input2.length == 0){
		error_msg = 'the password field is required';
		errors.push(error_msg);
		has_errors = true;
	}
	
	if(has_errors){

		alert('please fill the specified input fields with errors');
		var display_text = '<ul>';
		for (var i = 0; i < errors.length; i++) {
			display_text += '<li>'+errors[i]+'</li>';
		}
		display_text += '</ul>'; 
		$('#display-errors').html(display_text);
		return false;

	}else{
		form.submit();
	}
});


$('#Register-submit').click(function(){
	
	var input1, input2, input3,errors=[], has_errors=false;
	var error_msg="";
	input1 = $('#InputEmail').val();
	input1 = input1.replace(/\s/g,"");
	input2 = $('#Password').val();
	input3 = $('#ConfirmPassword').val();
	
	if( input1.length == 0 || !emailTest(input1)){
		if(input1.length == 0){
			error_msg = 'the email field is required';
			errors.push(error_msg);
		}
		if(!emailTest(input1)){
			error_msg = 'please enter a valid email address';
			errors.push(error_msg);
		}
		has_errors = true;
	}
	if(input2.length == 0){
		error_msg = 'the password field is required';
		errors.push(error_msg);
		has_errors = true;
	}
	if(input3.length == 0){
		error_msg = 'the confirm password field is required';
		errors.push(error_msg);
		has_errors = true;
	}
	if (input2!==input3) {
		error_msg = 'password and confirm password must match';
		errors.push(error_msg);
		has_errors = true;
	}
	
	if(has_errors){

		alert('please fill the specified input fields with errors');
		var display_text = '<ul>';
		for (var i = 0; i < errors.length; i++) {
			display_text += '<li>'+errors[i]+'</li>';
		}
		display_text += '</ul>'; 
		$('#display-errors').html(display_text);
		return false;

	}else{
		form.submit();
	}
});
	
function emailTest(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}


