        <!DOCTYPE html>
        <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- CSRF Token -->
            <meta name="csrf-token" content="{{ csrf_token() }}">

            <title>{{ config('app.name', 'Movie-cafe') }}</title>
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
            <link href="{{ asset('css/style.css') }}" rel="stylesheet">
            <link href="{{ asset('css/style2.css') }}" rel="stylesheet">

            <!-- Scripts -->
                <script src="{{ asset('js/app.js') }}" defer></script>
                <script src="{{ asset('js/jquery-3.2.1.min.js') }}" defer></script>
                <script src="{{ asset('js/popper.min.js') }}" defer></script>
                <script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
                <script src="{{ asset('js/func.js') }}" defer></script>
        </head>
        <body>
        <div class="custom-modal" id="custom-modal-2">
            <div class="row mt-5">
                <div class="col-sm-10 mx-auto p-2 bg-light">
                    <form class="animate insert" id="crudform" method="post" enctype="multipart/form-data" >

                        <div class="d-flex justify-content-between align-items-center">
                            <span class="font-weight-bold">Upload new video</span>
                            <div>

                                <button class="cancelTwo btn btn-danger btn-sm p-2 float-right" type="button">&times;</button>
                            </div>
                        </div> 
                        <hr />
                        
                            <input type="text" name="description" id="description" class="email form-control mb-2"  placeholder="Enter video description">
                            <input type="file" name="filename" id="video" class="title form-control mb-2" >
                            <input type="number"  name="rating" id="rating" class="address form-control mb-2" placeholder="rating">
                            <input type="hidden" name="mail" id="email" value="">
                            <input type="hidden" name="action" id="action" value="insert" />
                            
                        <div class="text-danger" id="error-div"></div>
                        <div class="text-success" id="success-div"></div>
                        <div class="text-right">
                            <button class="btn btn-primary btn-sm btnn" id="post-button">Submit</button>
                        </div> 
                    </form> 
                    </div>
                </div>
            </div>
        </div>
        <div class="container">

        	
        </div>

        	<div class="table-responsive">
             <button class="btn btn-outline-secondary btn-block rent"style="width:50px;">add</button>
            <table class="table table-bordered table-striped">
             <thead>
              <tr>
               <th>Video url</th>
               <th>description</th>
                <th>Rating</th>
               <th>Edit</th>
               <th>Delete</th>
              </tr>
             </thead>
             <tbody></tbody>
            </table>
             <div class="text-success delete-success" id="delete-div"></div>
           </div>
        </div>
        	

        </body>


   
    